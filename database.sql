CREATE DATABASE  IF NOT EXISTS myblogdb /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE myblogdb;

DROP TABLE IF EXISTS visits;
DROP TABLE IF EXISTS stars;
DROP TABLE IF EXISTS likes;
DROP TABLE IF EXISTS comments;
DROP TABLE IF EXISTS posts;
DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS categories;

CREATE TABLE categories (
  id int NOT NULL AUTO_INCREMENT,
  name varchar(255) NOT NULL,
  ref_model varchar(255) DEFAULT 'internal',
  ref_id varchar(64) DEFAULT NULL,
  ref_url varchar(255) DEFAULT NULL,
  scope varchar(16) DEFAULT 'public',
  status tinyint DEFAULT '1',
  created_at datetime DEFAULT NULL,
  updated_at datetime DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;

CREATE TABLE users (
  id int NOT NULL AUTO_INCREMENT,
  name varchar(255) NOT NULL,
  surname varchar(255) DEFAULT NULL,
  email varchar(255) DEFAULT NULL,
  password varchar(64) NOT NULL,
  role varchar(255) DEFAULT NULL,
  description tinytext,
  image varchar(255) DEFAULT NULL,
  created_at datetime DEFAULT NULL,
  updated_at datetime DEFAULT NULL,
  remember_token varchar(255) DEFAULT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY email_UNIQUE (email)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4;

CREATE TABLE posts (
  id int NOT NULL AUTO_INCREMENT,
  user_id int NOT NULL,
  category_id int NOT NULL,
  title varchar(255) NOT NULL,
  content text,
  image varchar(255) DEFAULT NULL,
  flags varchar(255) DEFAULT NULL,
  due_date datetime DEFAULT NULL,
  status tinyint DEFAULT '1',
  created_at datetime DEFAULT NULL,
  updated_at datetime DEFAULT NULL,
  PRIMARY KEY (id),
  KEY fk_posts_users_idx (user_id),
  KEY fk_posts_categories1_idx (category_id),
  CONSTRAINT fk_posts_categories1 FOREIGN KEY (category_id) REFERENCES categories (id) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT fk_posts_users FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4;

CREATE TABLE comments (
  id int NOT NULL AUTO_INCREMENT,
  post_id int NOT NULL,
  user_id int NOT NULL,
  parent_id int DEFAULT NULL,
  depth int DEFAULT '1',
  content varchar(4000) NOT NULL,
  status tinyint DEFAULT '1',
  created_at datetime DEFAULT NULL,
  updated_at datetime DEFAULT NULL,
  PRIMARY KEY (id),
  KEY fk_author_id_idx (user_id),
  KEY fk_com_post_id_idx (post_id),
  KEY fk_com_parent_id_idx (parent_id),
  CONSTRAINT fk_com_parent_id FOREIGN KEY (parent_id) REFERENCES comments (id) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT fk_com_post_id FOREIGN KEY (post_id) REFERENCES posts (id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT fk_com_user_id FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE likes (
  comment_id int NOT NULL,
  user_id int NOT NULL,
  status tinyint NOT NULL DEFAULT '1',
  created_at datetime DEFAULT NULL,
  updated_at datetime DEFAULT NULL,
  PRIMARY KEY (comment_id,user_id),
  KEY fk_like_userid_idx (user_id),
  KEY fk_like_commentid_idx (comment_id),
  CONSTRAINT fk_like_commentid FOREIGN KEY (comment_id) REFERENCES comments (id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT fk_like_userid FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE stars (
  user_id int NOT NULL,
  post_id int NOT NULL,
  stars smallint NOT NULL,
  created_at datetime DEFAULT NULL,
  updated_at datetime DEFAULT NULL,
  PRIMARY KEY (user_id,post_id),
  KEY idx_sta_posts (post_id),
  CONSTRAINT fk_sta_postid FOREIGN KEY (post_id) REFERENCES posts (id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT fk_sta_userid FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE visits (
  id int NOT NULL AUTO_INCREMENT,
  post_id INT NOT NULL,
  user_session VARCHAR(255) NOT NULL,
  user_id INT NULL DEFAULT NULL,
  user_ip VARCHAR(64) NULL DEFAULT NULL,
  read_at DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (id),
  INDEX fk_vis_postid_idx (post_id ASC),
  INDEX fk_vis_userid_idx (user_id ASC),
  CONSTRAINT fk_vis_postid FOREIGN KEY (post_id) REFERENCES mydb.posts (id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT fk_vis_userid FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
