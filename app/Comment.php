<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = 'comments';

    //Relación uno a muchos inversa (muchos a uno)
    public function post(){
        return $this->belongsTo('App\Post', 'post_id');
    }

    public function user(){
        return $this->belongsTo('App\User', 'user_id');
    }

    public function parent(){
        return $this->belongsTo('App\Comment', 'parent_id');
    }
}
