<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Visit extends Model
{
    protected $table = 'visits';
    public $timestamps = false;

    //Relación uno a muchos inversa (muchos a uno)
    public function post(){
        return $this->belongsTo('App\Post', 'post_id');
    }

    public function user(){
        return $this->belongsTo('App\User', 'user_id');
    }
}
