<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Post;
use App\Star;

class PostController extends Controller {

    public $MAX_DELETE_TIME = 60*60; //1 hora

    public function __construct() {
        $this->middleware('api.auth', ['except' => ['index', 'show', 'postsByUser', 'postsByCategory', 'postNext', 'getImage']]);
    }

    public function index(Request $request) {
        // Obtener entradas activas y superiores a 1 año de actualizadas
        $posts = Post::where('status', 1)
            ->where('updated_at', '>=', date('Y-m-d', strtotime('-1 year')))
            ->where(function ($query) {
                $query
                    ->whereNull('due_date')
                    ->orWhere('due_date', '>=', date('Y-m-d'));
            })
            ->orderBy('updated_at', 'DESC')
            ->get();
        if ($posts && is_object($posts) && !$posts->isEmpty()) {
            $posts->load('category');
            $data = array(
                'status'    => 'success',
                'code'      => 200,
                'posts'      => $posts
            );    

        }
        else {
            $data = array(
                'status'    => 'error',
                'code'      => 404,
                'message'   => 'Entradas no encontradas.',
                'posts'      => '{}'
            );    

        }

        return response()->json($data, (isset($data['code']))? $data['code'] : 200 );
    }

    public function show($id) {
        // Obtener la entrada con el id definido
        $post = Post::find($id);
        if ($post && is_object($post)) {
            $post->load('category')
                ->load('user');

            $data = array(
                'status'    => 'success',
                'code'      => 200,
                'post'      => $post
            );    

        }
        else {
            $data = array(
                'status'    => 'error',
                'code'      => 404,
                'message'   => 'Entrada no encontrada.',
            );    

        }

        return response()->json($data, (isset($data['code']))? $data['code'] : 200 );

    }

    public function store(Request $request) {
        // Recoger los datos por post
        $json = $request->input('json', null);
        $params = json_decode($json, true);

        // Obtener identidad de usuario autenticado
        $jwt = $request->header('Authorization');
        $token = new \JwtAuth();
        $identity = $token->checkToken($jwt, true);
        if ($identity) {
            $user_id = $identity->sub;

            if (!empty($params)) {
                // Limpiar espacios sobrantes en los datos
                $params = array_map('trim', $params);
    
                // Validar los datos
                $validate = \Validator::make($params, [
                    'title'         => 'required|max:255',
                    'content'       => 'required',
    //                'image'      => 'required|image|mimes:png,jpg,jpeg,gif',
                    'due_date'      => 'nullable|date',
                    'category_id'   => 'required|exists:categories,id'
                ]);
    
                if ($validate->fails()) {
                    $data = array(
                        'status'    => 'error',
                        'code'      => 400,
                        'message'   => 'La entrada no ha sido registrada. Por favor verifique los datos suministrados.',
                        'errors'     => $validate->errors()
                    );    
                }
                else {
                    // Guardar la categoría
                    $post = new Post();
                    $post->title = $params['title'];
                    $post->status = 1;
                    if (isset($params['image'])) {
                        $post->image = $params['image'];
                    }
                    if (isset($params['due_date'])) {
                        $post->due_date = $params['due_date'];
                    }
                    $post->content = $params['content'];
                    $post->category_id = $params['category_id'];
                    $post->user_id = $user_id;
                    if ($post->save()) {
                        $data = array(
                            'status'    => 'success',
                            'code'      => 200,
                            'message'   => 'La entrada al blog ha sido registrada.',
                            'post'      => $post
                        );    
    
                    }
                    else {
                        $data = array(
                            'status'    => 'error',
                            'code'      => 500,
                            'message'   => 'La entrada no ha podido ser registrada en el sistema. Por favor consulte el administrador del servicio.',
                        );    
        
                    }
    
                }
            }
            else {
                $data = array(
                    'status'    => 'error',
                    'code'      => 400,
                    'message'   => 'Error en el formato de los datos enviados.',
                );
    
            }
    
        }
        else{
            $data = array(
                'status'    => 'error',
                'code'      => 400,
                'message'   => 'Identidad de usuario desconocida.',
            );

        }

        // Devolver el resultado
        return response()->json($data, (isset($data['code']))? $data['code'] : 200 );
    }

    public function update($id, Request $request) {
        // Recoger los datos por post
        $json = $request->input('json', null);
        $params = json_decode($json, true);

        if (!empty($params)) {
            // Limpiar espacios sobrantes en los datos
            $params = array_map('trim', $params);

            // Validar los datos
            $validate = \Validator::make($params, [
                'title'         => 'required|max:255',
                'content'       => 'required',
                'due_date'      => 'nullable|date',
                'category_id'       => 'required|exists:categories,id'
            ]);

            if ($validate->fails()) {
                $data = array(
                    'status'    => 'error',
                    'code'      => 400,
                    'message'   => 'La entrada no ha sido actualizada. Por favor verifique los datos suministrados.',
                    'errors'     => $validate->errors()
                );    
            }
            else {
                // Actualizar la entrada
                $post = Post::find($id);
                if ($post && is_object($post)) {
                    // Obtener identidad de usuario autenticado
                    $jwt = $request->header('Authorization');
                    $token = new \JwtAuth();
                    $identity = $token->checkToken($jwt, true);
                    // Solo permite actualizar al usuario quien lo registró o al administrador
                    if (($post->user_id == $identity->sub) || ($identity->role =='ROLE-ADMIN')) {
                        $post->title = $params['title'];
                        $post->content = $params['content'];
                        $post->status = 1;
                        if (isset($params['due_date'])) {
                            $post->due_date = $params['due_date'];
                        }
                        if (isset($params['image'])) {
                            $post->image = $params['image'];
                        }
                        $post->category_id = $params['category_id'];
                        if ($post->save()) {
                            $data = array(
                                'status'    => 'success',
                                'code'      => 200,
                                'message'   => 'La entrada ha sido actualizada.',
                                'post'      => $post
                            );    
        
                        }
                        else {
                            $data = array(
                                'status'    => 'error',
                                'code'      => 500,
                                'message'   => 'La entrada no ha podido ser actualizada en el sistema. Por favor consulte el administrador del servicio.',
                            );    
            
                        }
    
                    }
                    else {
                        $data = array(
                            'status'    => 'error',
                            'code'      => 404,
                            'message'   => 'Usted no tiene los permisos para modificar la entrada definida',
                        );    
            
                    }
                }
                else {
                    $data = array(
                        'status'    => 'error',
                        'code'      => 404,
                        'message'   => 'Entrada no encontrada para su actualizacion.',
                    );    
        
                }
            }
        }
        else {
            $data = array(
                'status'    => 'error',
                'code'      => 400,
                'message'   => 'Error en el formato de los datos enviados.',
            );

        }
        // Devolver el resultado
        return response()->json($data, (isset($data['code']))? $data['code'] : 200 );

    }

    public function destroy($id, Request $request) {
        // Encontrar la entrada
        $post = Post::find($id);
        if ($post && is_object($post)) {
            // Obtener identidad de usuario autenticado
            $jwt = $request->header('Authorization');
            $token = new \JwtAuth();
            $identity = $token->checkToken($jwt, true);
            if (($post->user_id == $identity->sub) || ($identity->role =='ROLE-ADMIN')) {
                // Inactivar si el registro es superior a 1 hora
                if ((time() - strtotime($post->created_at)) > ($this->MAX_DELETE_TIME)) {
                    $data = array(
                        'status'    => 'success',
                        'code'      => 200,
                        'message'   => "Entrada $id inactivada"
                    );        
                    $post->status = 0;
                    $post->save();

                }
                // Eliminar si el registro es inferior a 1 hora
                elseif ($post->delete()) {
                    $data = array(
                        'status'    => 'success',
                        'code'      => 200,
                        'message'   => "Entrada $id borrada"
                    );        
                }
                else {
                    $data = array(
                        'status'    => 'error',
                        'code'      => 500,
                        'message'   => 'La entrada no ha podido ser eliminada del sistema. Por favor consulte el administrador del servicio.',
                    );    
    
                }
            }
            else {
                $data = array(
                    'status'    => 'error',
                    'code'      => 404,
                    'message'   => 'Usted no tiene los permisos para modificar la entrada definida',
                );    

            }

        }
        else {
            $data = array(
                'status'    => 'error',
                'code'      => 404,
                'message'   => 'Entrada no encontrada.',
            );    

        }

        return response()->json($data, (isset($data['code']))? $data['code'] : 200 );
    }

    public function postsByCategory($id) {
        // Obtener entradas activas y superiores a 1 año de actualizadas
        $posts = Post::where('status', 1)
            ->where('updated_at', '>=', date('Y-m-d', strtotime('-1 year')))
            ->where(function ($query) {
                $query
                    ->whereNull('due_date')
                    ->orWhere('due_date', '>=', date('Y-m-d'));
            })
            ->where('category_id', $id)
            ->orderBy('updated_at', 'DESC')
            ->get();
        if ($posts && is_object($posts) && !$posts->isEmpty()) {
            $posts->load('category');
            $data = array(
                'status'    => 'success',
                'code'      => 200,
                'posts'     => $posts
            );    

        }
        else {
            $data = array(
                'status'    => 'error',
                'code'      => 404,
                'message'   => 'Entradas no encontradas.',
                'posts'     => '{}'
            );    

        }

        return response()->json($data, (isset($data['code']))? $data['code'] : 200 );
    }

    public function postsByUser($id) {
        // Obtener entradas activas y superiores a 1 año de actualizadas
        $posts = Post::where('status', 1)
            ->where('updated_at', '>=', date('Y-m-d', strtotime('-1 year')))
            ->where(function ($query) {
                $query
                    ->whereNull('due_date')
                    ->orWhere('due_date', '>=', date('Y-m-d'));
            })
            ->where('user_id', $id)
            ->orderBy('updated_at', 'DESC')
            ->get();
        if ($posts && is_object($posts) && !$posts->isEmpty()) {
            $posts->load('category');
            $data = array(
                'status'    => 'success',
                'code'      => 200,
                'posts'      => $posts
            );    

        }
        else {
            $data = array(
                'status'    => 'error',
                'code'      => 404,
                'message'   => 'Entradas no encontradas.',
                'posts'      => '{}'
            );    

        }

        return response()->json($data, (isset($data['code']))? $data['code'] : 200 );
    }

    public function postsByCourse($course_id, $nb_days = 30) {
        // Validación de la entrada de datos
        if (!is_numeric($nb_days) || $nb_days > 90 || $nb_days < 1) {
            $data = array(
                'status'    => 'error',
                'code'      => 404,
                'message'   => 'Error en la especificación de días de la consulta. Se espera un número no superior a 90 días'
            );    

        }
        else {
            // Obtener entradas activas y superiores a 1 año de actualizadas
            $posts = Post::where('status', 1)
                ->where('updated_at', '>=', date('Y-m-d', strtotime("- $nb_days day")))
                ->where(function ($query) { $query->whereNull('due_date')->orWhere('due_date', '>=', date('Y-m-d')); })
                ->orderBy('updated_at', 'DESC')
                ->get()
                ->where('category.ref_id', $course_id);
            /*
            TODO: Mejorar consulta para hacerla desde la base de datos
            $posts = Post::with(['category' => function ($query) use ($course_id) {
                    $query->where('ref_id', $course_id);
                } ])
                ->where('status', 1)
                ->where('updated_at', '>=', date('Y-m-d', strtotime('-1 year')))
                ->where(function ($query) {
                    $query
                        ->whereNull('due_date')
                        ->orWhere('due_date', '>=', date('Y-m-d'));
                })
                ->orderBy('updated_at', 'DESC')
                ->get();
            */
            if ($posts && is_object($posts) && !$posts->isEmpty()) {
                $posts->load('category');
                $data = array(
                    'status'    => 'success',
                    'code'      => 200,
                    'posts'     => $posts
                );    

            }
            else {
                $data = array(
                    'status'    => 'error',
                    'code'      => 404,
                    'message'   => 'Entradas no encontradas.',
                    'posts'     => '{}'
                );    

            }
            
        }

        return response()->json($data, (isset($data['code']))? $data['code'] : 200 );
    }

    public function postNext($id, $dir) {
        // Validar direccion del próximo post a consultar
        if ($dir == 'up' || $dir == 'down') {
            // Obtener entrada actual
            $current_post = Post::find($id);
            if (!empty($current_post) && $current_post->id) {
                $date_ref = $current_post->updated_at;
                if (!$date_ref) {
//                    $date_ref = date('Y-m-d');
                }
                if ($dir == 'up') {
                    // Obtener siguiente entrada hacia arriba
                    $post = Post::where('status', 1)
                        ->where('updated_at', '>', $date_ref)
                        ->where(function ($query) {
                            $query
                                ->whereNull('due_date')
                                ->orWhere('due_date', '>=', date('Y-m-d'));
                        })
                        ->orderBy('updated_at', 'ASC')
                        ->first();
                }
                else {
                    // Obtener siguiente entrada hacia abajo
                    $post = Post::where('status', 1)
                    ->where('updated_at', '<', $date_ref)
                    ->where(function ($query) {
                        $query
                            ->whereNull('due_date')
                            ->orWhere('due_date', '>=', date('Y-m-d'));
                    })
                    ->orderBy('updated_at', 'DESC')
                    ->first();
                }
                if ($post && is_object($post)) {
                    $post->load('category')->load('user');
                    $data = array(
                        'status'    => 'success',
                        'code'      => 200,
                        'post'      => $post
                    );    

                }
                else {
                    $data = array(
                        'status'    => 'error',
                        'code'      => 404,
                        'message'   => 'Entrada no encontrada.',
                    );    

                }
            }
            else {
                $data = array(
                    'status'    => 'error',
                    'code'      => 404,
                    'message'   => 'No se encuentra la entrada de referencia para la navegacion.',
                );    
            }
    
        }
        else {
            $data = array(
                'status'    => 'error',
                'code'      => 404,
                'message'   => 'Direccion de la consulta errada, solo se permite "up" o "down".',
            );    
        }

        return response()->json($data, (isset($data['code']))? $data['code'] : 200 );
    }

    public function indexInactives(Request $request) {
        // Si el usuario está autenticado, permitir consultar también sus posts sin restricción de estado o tiempo
        $jwt = $request->header('Authorization');
        $token = new \JwtAuth();
        $identity = $token->checkToken($jwt, true);
        if ($identity) {
            // Obtener entradas inactivas y superiores a 1 año de actualizadas
            if ($identity->role == 'ROLE-ADMIN') {
                $posts = Post::where('status', 0)
                ->orWhere('updated_at', '<', date('Y-m-d', strtotime('-1 year')))
                ->orWhere('due_date', '<', date('Y-m-d'))
                ->orderBy('updated_at', 'DESC')
                ->get();
            }
            else {
                $posts = Post::where('user_id', $identity->sub)
                    ->where(
                        function ($query){
                            $query->where('status', 0)
                                ->orWhere('updated_at', '<', date('Y-m-d', strtotime('-1 year')))
                                ->orWhere('due_date', '<', date('Y-m-d'));
                        }
                    )
                    ->orderBy('updated_at', 'DESC')
                    ->get();
            }

            if ($posts && is_object($posts) && !$posts->isEmpty()) {
                $posts->load('category');
                $data = array(
                    'status'    => 'success',
                    'code'      => 200,
                    'posts'      => $posts
                );    
    
            }
            else {
                $data = array(
                    'status'    => 'error',
                    'code'      => 404,
                    'message'   => 'Entradas no encontradas.',
                    'posts'      => '{}'
                );
            }
        }
        else {
            $data = array(
                'status'    => 'error',
                'code'      => 400,
                'message'   => 'Identidad de usuario desconocida.',
            );
        }

        return response()->json($data, (isset($data['code']))? $data['code'] : 200 );
    }

    public function upload(Request $request) {
        // Recoger los datos de la petición
        $image = $request->file('file0');

        // Validar la imagen
        $validate = \Validator::make($request->all(),[
            'file0' => 'required|image|mimes:jpg,jpeg,png,gif'
        ]);

        if (!$image || $validate->fails()) {
            $data = array(
                'status'    => 'error',
                'code'      => 400,
                'message'   => 'Error al subir la imagen.',
                'errors'     => $validate->errors()
            );
        }
        else {
            // Guardar la imagen
            $image_name = time().$image->getClientOriginalName();
            \Storage::disk('images')->put($image_name, \File::get($image));

            // Devolver la ubicación de la imagen
            $data = array(
                'status'    => 'success',
                'code'      => 200,
                'message'   => 'Archivo cargado exitosamente.',
                'image'     => $image_name,
            );

        }

        return response()->json($data, (isset($data['code']))? $data['code'] : 200 );
    }

    public function getImage($filename) {
        // Validar si existe el archivo
        $isset = \Storage::disk('images')->exists($filename);

        if ($isset) {
            // Entregar flujo de archivo
            $file = \Storage::disk('images')->get($filename);

            return new Response($file, 200);
        }
        else {
            // Enviar mensaje de error
            $data = array(
                'status'    => 'error',
                'code'      => 404,
                'message'   => 'Archivo no encontrado.',
            );
            return response()->json($data, (isset($data['code']))? $data['code'] : 200 );
        }

    }

    public function getAverageStars($post_id){

        $stars_count = Star::where('post_id', $post_id)
            ->count();

        if ($stars_count > 0) {

            $stars_avg = Star::where('post_id', $post_id)
                ->avg('stars');

            $data = array(
                'status'    => 'success',
                'code'      => 200,
                'average'   => $stars_avg,
                'post_id' => $post_id
            );    

        }
        else {
            $data = array(
                'status'    => 'error',
                'code'      => 404,
                'message'   => 'No hay calificaciones sobre la entrada. Registra el primero.',
            );    

        }

        return response()->json($data, (isset($data['code']))? $data['code'] : 200 );
    }

    public function updateMyStars($post_id, Request $request) {
        // Recoger los datos por post
        $json = $request->input('json', null);
        $params = json_decode($json, true);

        // Obtener identidad de usuario autenticado
        $jwt = $request->header('Authorization');
        $token = new \JwtAuth();
        $identity = $token->checkToken($jwt, true);
        if ($identity) {
            $user_id = $identity->sub;

            if (!empty($params)) {
                // Limpiar espacios sobrantes en los datos
                $params = array_map('trim', $params);
    
                // Validar los datos
                $validate = \Validator::make($params, [
                    'stars'     => 'required',
                ]);
    
                if ($validate->fails()) {
                    $data = array(
                        'status'    => 'error',
                        'code'      => 400,
                        'message'   => 'La valoración no ha sido registrada. Por favor verifique los datos suministrados.',
                        'errors'    => $validate->errors()
                    );    
                }
                else {
                    // Revisar si existe valoración previa
                    $star = Star::where('post_id', $post_id)
                        ->where('user_id', $user_id)
                        ->first();
                    if ($star->count() > 0) {
                        $result = Star::where('post_id', $post_id)
                            ->where('user_id', $user_id)
                            ->update(['stars' => $params['stars']]);
                        $star = Star::where('post_id', $post_id)
                            ->where('user_id', $user_id)
                            ->first();
                    }
                    else {
                        $star = new Star();
                        $star->stars = $params['stars'];
                        $star->post_id = $post_id;
                        $star->user_id = $user_id;
                        $result = $star->save();                    
                    }
                    // Guardar la valoración
                    if ($result) {
                        $data = array(
                            'status'    => 'success',
                            'code'      => 200,
                            'message'   => 'La valoración ha sido registrada.',
                            'stars'     => $star,
                            'post_id' => $post_id
                        );
                    }
                    else {
                        $data = array(
                            'status'    => 'error',
                            'code'      => 500,
                            'message'   => 'La valoración no ha podido ser registrada en el sistema. Por favor consulte el administrador del servicio.',
                        );
                    }
                }
            }
            else {
                $data = array(
                    'status'    => 'error',
                    'code'      => 400,
                    'message'   => 'Error en el formato de los datos enviados.',
                );
    
            }
    
        }
        else {
            $data = array(
                'status'    => 'error',
                'code'      => 400,
                'message'   => 'Identidad de usuario desconocida.',
            );

        }

        // Devolver el resultado
        return response()->json($data, (isset($data['code']))? $data['code'] : 200 );
    }

}
