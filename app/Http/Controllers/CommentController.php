<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Comment;
use App\Like;

class CommentController extends Controller {

    public function __construct() {
        $this->middleware('api.auth', ['except' => []]);
    }

    public function getComments($post_id, $depth = 1) {
        // Obtener comentarios de una entrada específica iniciales
        $comments = Comment::where('post_id', $post_id)
            ->where('status', 1)
            ->where('depth', $depth)
            ->orderBy('updated_at', 'DESC')
            ->get();
        if ($comments && is_object($comments) && !$comments->isEmpty()) {
            $comments->load('user');
            $data = array(
                'status'    => 'success',
                'code'      => 200,
                'comments'      => $comments
            );    

        }
        else {
            $data = array(
                'status'    => 'error',
                'code'      => 404,
                'message'   => 'Comentarios no encontrados.',
                'comments'      => '{}'
            );    

        }

        return response()->json($data, (isset($data['code']))? $data['code'] : 200 );
    }

    public function getComment($id) {
        $comment = Comment::find($id);
        if ($comment && is_object($comment)) {
            $data = array(
                'status'    => 'success',
                'code'      => 200,
                'comment'   => $comment
            );    

        }
        else {
            $data = array(
                'status'    => 'error',
                'code'      => 404,
                'message'   => 'Comentario no encontrado.',
            );    

        }

        return response()->json($data, (isset($data['code']))? $data['code'] : 200 );
    }

    public function getCommentsByParent($parent_id) {
        $comments = Comment::where('parent_id', $parent_id)
            ->where('status', 1)
            ->orderBy('updated_at', 'DESC')
            ->get();
        if ($comments && is_object($comments) && !$comments->isEmpty()) {
            $data = array(
                'status'    => 'success',
                'code'      => 200,
                'comments'   => $comments
            );    

        }
        else {
            $data = array(
                'status'    => 'error',
                'code'      => 404,
                'message'   => 'Comentario no encontrado.',
            );    

        }

        return response()->json($data, (isset($data['code']))? $data['code'] : 200 );
    }

    public function newComment($post_id, Request $request) {
        // Recoger los datos por post
        $json = $request->input('json', null);
        $params = json_decode($json, true);

        // Obtener identidad de usuario autenticado
        $jwt = $request->header('Authorization');
        $token = new \JwtAuth();
        $identity = $token->checkToken($jwt, true);
        if ($identity) {
            $user_id = $identity->sub;

            if (!empty($params)) {
                // Limpiar espacios sobrantes en los datos
                $params = array_map('trim', $params);
    
                // Validar los datos
                $validate = \Validator::make($params, [
                    'content'     => 'required',
                    'parent_id'   => 'exists:comments,id'
                ]);
    
                if ($validate->fails()) {
                    $data = array(
                        'status'    => 'error',
                        'code'      => 400,
                        'message'   => 'El comentario no ha sido registrado. Por favor verifique los datos suministrados.',
                        'errors'    => $validate->errors()
                    );    
                }
                else {
                    // Guardar el comentario
                    $comment = new Comment();
                    $comment->content = $params['content'];
                    if (isset($params['parent_id'])) {
                        $comment->parent_id = $params['parent_id'];
                    }
                    if (isset($params['depth']) && is_numeric($params['depth'])) {
                        $comment->depth = $params['depth'];
                    }
                    else {
                        $comment->depth = 1;
                    }
                    $comment->post_id = $post_id;
                    $comment->user_id = $user_id;
                    $comment->status = 1;
                    if ($comment->save()) {
                        $data = array(
                            'status'    => 'success',
                            'code'      => 200,
                            'message'   => 'El comentario ha sido registrado.',
                            'comment'   => $comment
                        );
                    }
                    else {
                        $data = array(
                            'status'    => 'error',
                            'code'      => 500,
                            'message'   => 'El comentario no ha podido ser registrado en el sistema. Por favor consulte el administrador del servicio.',
                        );
                    }
                }
            }
            else {
                $data = array(
                    'status'    => 'error',
                    'code'      => 400,
                    'message'   => 'Error en el formato de los datos enviados.',
                );
    
            }
    
        }
        else {
            $data = array(
                'status'    => 'error',
                'code'      => 400,
                'message'   => 'Identidad de usuario desconocida.',
            );

        }

        // Devolver el resultado
        return response()->json($data, (isset($data['code']))? $data['code'] : 200 );
    }

    public function destroyComment($id, Request $request){
        // Encontrar la entrada
        $comment = Comment::find($id);
        if ($comment && is_object($comment)){
            // Obtener identidad de usuario autenticado
            $jwt = $request->header('Authorization');
            $token = new \JwtAuth();
            $identity = $token->checkToken($jwt, true);
            if (($comment->user_id == $identity->sub) || ($identity->role =='ROLE-ADMIN')) {
                $child = Comment::where('parent_id', $id)
                    ->first();
                if ($child && is_object($child)) {
                    $data = array(
                        'status'    => 'error',
                        'code'      => 400,
                        'message'   => 'El comentario no ha podido ser eliminado del sistema. Elimine primero los comentarios de respuesta derivados.',
                    );    

                }
                elseif ($comment->delete()) {
                    $data = array(
                        'status'    => 'success',
                        'code'      => 200,
                        'message'   => "Entrada $id borrada"
                    );        
                }
                else {
                    $data = array(
                        'status'    => 'error',
                        'code'      => 500,
                        'message'   => 'El comentario no ha podido ser eliminado del sistema. Por favor consulte el administrador del servicio.',
                    );    
    
                }
            }
            else {
                $data = array(
                    'status'    => 'error',
                    'code'      => 403,
                    'message'   => 'Usted no tiene los permisos para eliminar el comentario definido.',
                );    

            }

        }
        else{
            $data = array(
                'status'    => 'error',
                'code'      => 404,
                'message'   => 'Comentario no encontrado.',
            );    

        }

        return response()->json($data, (isset($data['code']))? $data['code'] : 200 );
    }

    public function getSumCommentLikes($comment_id){

        $likes_count = Like::where('comment_id', $comment_id)
            ->where('status', 1)
            ->count();

        if ($likes_count > 0) {

            $data = array(
                'status'    => 'success',
                'code'      => 200,
                'sum'   => $likes_count,
                'comment_id' => $comment_id
            ); 
        }
        else {
            $data = array(
                'status'    => 'error',
                'code'      => 404,
                'message'   => 'No hay likes sobre el comentario.',
            );    

        }

        return response()->json($data, (isset($data['code']))? $data['code'] : 200 );
    }

    public function likeComment($comment_id, Request $request) {
        // Recoger los datos por post
        $json = $request->input('json', null);

        // Obtener identidad de usuario autenticado
        $jwt = $request->header('Authorization');
        $token = new \JwtAuth();
        $identity = $token->checkToken($jwt, true);
        if ($identity) {
            $user_id = $identity->sub;

            // Revisar si existe valoración previa
            $like_count = Like::where('comment_id', $comment_id)
                ->where('user_id', $user_id)
                ->count();
            if (!$like_count) {
                $like = new Like();
                $like->comment_id = $comment_id;
                $like->user_id = $user_id;
                $like->status = 1;

                // Guardar la valoración
                $result = $like->save();
            }
            else {
                $result = Like::where('comment_id', $comment_id)
                    ->where('user_id', $user_id)
                    ->update(['status' => 1]);
            }    
            if ($result) {
                $data = array(
                    'status'    => 'success',
                    'code'      => 200,
                    'message'   => 'Comentario valorado.',
                    'comment_id' => $comment_id
                );
            }
            else {
                $data = array(
                    'status'    => 'error',
                    'code'      => 500,
                    'message'   => 'La valoración no ha podido ser registrada en el sistema. Por favor consulte el administrador del servicio.',
                );
            }

        }
        else {
            $data = array(
                'status'    => 'error',
                'code'      => 400,
                'message'   => 'Identidad de usuario desconocida.',
            );
        }
        // Devolver el resultado
        return response()->json($data, (isset($data['code']))? $data['code'] : 200 );
    }

    public function unlikeComment($comment_id, Request $request) {
        // Recoger los datos por post
        $json = $request->input('json', null);

        // Obtener identidad de usuario autenticado
        $jwt = $request->header('Authorization');
        $token = new \JwtAuth();
        $identity = $token->checkToken($jwt, true);
        if ($identity) {
            $user_id = $identity->sub;

            // Revisar si existe valoración previa
            $like = Like::where('comment_id', $comment_id)
                ->where('user_id', $user_id)
                ->first();
            if ($like && is_object($like)) {
                $result = Like::where('comment_id', $comment_id)
                    ->where('user_id', $user_id)
                    ->update(['status' => 0]);
                if ($result) {
                    $data = array(
                        'status'    => 'success',
                        'code'      => 200,
                        'message'   => 'Valoración suprimida.',
                        'comment_id' => $comment_id
                    );
                }
                else {
                    $data = array(
                        'status'    => 'error',
                        'code'      => 500,
                        'message'   => 'La valoración no ha podido ser suprimida en el sistema. Por favor consulte el administrador del servicio.',
                    );
                }
            }
            else {
                $data = array(
                    'status'    => 'error',
                    'code'      => 400,
                    'message'   => 'No se identificó la valoración del usuario sobre el comentario.',
                );
            }
        }
        else {
            $data = array(
                'status'    => 'error',
                'code'      => 400,
                'message'   => 'Identidad de usuario desconocida.',
            );
        }
        // Devolver el resultado
        return response()->json($data, (isset($data['code']))? $data['code'] : 200 );
    }

}