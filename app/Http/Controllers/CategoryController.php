<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Post;

class CategoryController extends Controller
{

    public function __construct() {
        $this->middleware('api.auth', ['except' => ['index', 'show']]);
    }

    public function index() {
        $categories = Category::all();
        if ($categories && is_object($categories)) {
            $data = array(
                'status'    => 'success',
                'code'      => 200,
                'categories'      => $categories
            );    

        }
        else {
            $data = array(
                'status'    => 'error',
                'code'      => 404,
                'message'   => 'Categorías no encontradas.',
                'categories'      => '{}'
            );    

        }

        return response()->json($data, (isset($data['code']))? $data['code'] : 200 );
    }

    public function show($id) {
        $category = Category::find($id);
        if ($category && is_object($category)) {
            $data = array(
                'status'    => 'success',
                'code'      => 200,
                'category'      => $category
            );    

        }
        else {
            $data = array(
                'status'    => 'error',
                'code'      => 404,
                'message'   => 'Categoría no encontrada.',
            );    

        }

        return response()->json($data, (isset($data['code']))? $data['code'] : 200 );
    }

    public function store(Request $request) {
        // Recoger los datos por post
        $json = $request->input('json', null);
        $params = json_decode($json, true);

        if (!empty($params)) {
            // Limpiar espacios sobrantes en los datos
            $params = array_map('trim', $params);

            // Validar los datos
            $validate = \Validator::make($params, [
                'name'      => 'required|max:255',
            ]);

            if ($validate->fails()) {
                $data = array(
                    'status'    => 'error',
                    'code'      => 400,
                    'message'   => 'La categoria no ha sido registrada. Por favor verifique los datos suministrados.',
                    'errors'     => $validate->errors()
                );    
            }
            else {
                // Guardar la categoría
                $category = new Category();
                $category->name = $params['name'];
                $category->status = 1;
                if ($category->save()) {
                    $data = array(
                        'status'    => 'success',
                        'code'      => 200,
                        'message'   => 'La categoria ha sido registrada.',
                        'category'      => $category
                    );    

                }
                else {
                    $data = array(
                        'status'    => 'error',
                        'code'      => 500,
                        'message'   => 'La categoria no ha podido ser registrada en el sistema. Por favor consulte el administrador del servicio.',
                    );    
    
                }

            }
        }
        else {
            $data = array(
                'status'    => 'error',
                'code'      => 400,
                'message'   => 'Error en el formato de los datos enviados.',
            );

        }

        // Devolver el resultado
        return response()->json($data, (isset($data['code']))? $data['code'] : 200 );
    }

    public function update($id, Request $request) {
        // Recoger los datos por post
        $json = $request->input('json', null);
        $params = json_decode($json, true);

        if (!empty($params)) {
            // Limpiar espacios sobrantes en los datos
            $params = array_map('trim', $params);

            // Validar los datos
            $validate = \Validator::make($params, [
                'name'      => 'required|max:255',
            ]);

            if ($validate->fails()) {
                $data = array(
                    'status'    => 'error',
                    'code'      => 400,
                    'message'   => 'La categoria no ha sido actualizada. Por favor verifique los datos suministrados.',
                    'errors'     => $validate->errors()
                );    
            }
            else {
                // Obtener la categoría
                unset($params['id']);
                unset($params['created_at']);
                $params['status'] = 1;
                $category = Category::where('id', $id)->update($params);
                // $category = Category::find($id);
                // $category->name = $params['name'];
                // if ($category->save()) {
                if ($category) {
                    $data = array(
                        'status'    => 'success',
                        'code'      => 200,
                        'message'   => 'La categoria ha sido actualizada.',
                        'category'      => $params
                    );    

                }
                else {
                    $data = array(
                        'status'    => 'error',
                        'code'      => 500,
                        'message'   => 'La categoria no ha podido ser actualizada en el sistema. Por favor consulte el administrador del servicio.',
                    );    
    
                }

            }
        }
        else {
            $data = array(
                'status'    => 'error',
                'code'      => 400,
                'message'   => 'Error en el formato de los datos enviados.',
            );

        }
        // Devolver el resultado
        return response()->json($data, (isset($data['code']))? $data['code'] : 200 );
    }

    public function destroy($id) {
        if (!empty($id)) {
            $posts_nb = Post::where('category_id', $id)->count();
            if ($posts_nb > 0) {
                // Si tiene entradas asociadas, entonces solo debe inactivar la categoría
                if (Category::where('id', $id)->update(['status' => 0])) {
                    $data = array(
                        'status'    => 'success',
                        'code'      => 200,
                        'message'   => "Categoria $id inactivada"
                    );    

                }
                else {
                    $data = array(
                        'status'    => 'error',
                        'code'      => 500,
                        'message'   => 'Categoria no inactivada. Por favor consulte su administrador',
                    );    
        
                }
            }
            else {
                // Como no tienen entradas, se procede a eliminar la categoría
                $category = Category::find($id);
                if (!empty($category)) {
                    if($category->delete()){
                        $data = array(
                            'status'    => 'success',
                            'code'      => 200,
                            'message'   => "Categoria $id borrada"
                        );    
            
                    }
                    else {
                        $data = array(
                            'status'    => 'error',
                            'code'      => 500,
                            'message'   => 'Categoria no eliminada. Por favor consulte su administrador',
                        );    
            
                    }
    
                }
                else {
                    $data = array(
                        'status'    => 'error',
                        'code'      => 404,
                        'message'   => 'Categoria no encontrada.',
                    );    
        
                }
    
            }
    
        }
        else {
            $data = array(
                'status'    => 'error',
                'code'      => 400,
                'message'   => 'Categoría no especificada.',
            );    

        }

        return response()->json($data, (isset($data['code']))? $data['code'] : 200 );
    }

    public function storeReferenceCourse(Request $request){
        // Recoger los datos por post
        $json = $request->input('json', null);
        $params = json_decode($json, true);

        if (!empty($params)) {
            // Limpiar espacios sobrantes en los datos
            $params = array_map('trim', $params);

            // Validar los datos
            $validate = \Validator::make($params, [
                'name'          => 'required|max:255|unique:categories,name',
                'external_id'   => 'required|max:64|unique:categories,ref_id',
                'external_url'  => 'required|max:255|url',
                'scope'         => 'max:16|alpha',
            ]);

            if ($validate->fails()) {
                $data = array(
                    'status'    => 'error',
                    'code'      => 400,
                    'message'   => 'La categoria no ha sido registrada. Por favor verifique los datos suministrados.',
                    'errors'     => $validate->errors()
                );    
            }
            else {
                // Guardar la categoría
                $category = new Category();
                $category->name = $params['name'];
                $category->ref_model = 'course';
                $category->ref_id = $params['external_id'];
                $category->ref_url = $params['external_url'];
                $category->scope = $params['scope'];
                $category->status = 1;
                if($category->save()){
                    $data = array(
                        'status'    => 'success',
                        'code'      => 200,
                        'message'   => 'La categoria ha sido registrada.',
                        'category'      => $category
                    );    

                }
                else {
                    $data = array(
                        'status'    => 'error',
                        'code'      => 500,
                        'message'   => 'La categoria no ha podido ser registrada en el sistema. Por favor consulte el administrador del servicio.',
                    );    
    
                }

            }
        }
        else {
            $data = array(
                'status'    => 'error',
                'code'      => 400,
                'message'   => 'Error en el formato de los datos enviados.',
            );

        }

        // Devolver el resultado
        return response()->json($data, (isset($data['code']))? $data['code'] : 200 );
    }

}
