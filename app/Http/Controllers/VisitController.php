<?php

namespace App\Http\Controllers;

use Illuminate\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Visit;

class VisitController extends Controller {

    public function storeVisit($id, Request $request) {
        // Validar el identificador del post
        $post_id = null;
        $post = DB::table('posts')->select('id')->where('id',$id)->first();
        if ($post && is_object($post)) {
            $post_id = $post->id;
            // Datos de la sesión de usuario:
            $session_id = session()->getId();
            // Obtener identidad de usuario autenticado
            $user_id = null;
            $jwt = $request->header('Authorization');
            $token = new \JwtAuth();
            $identity = $token->checkToken($jwt, true);
            if ($identity) {
                $user_id = $identity->sub;
            }
            // Obtener la IP
            $user_ip = request()->ip();
            // Definir la fecha y hora de registro
            $read_at = date('Y-m-d H:i');

            $visit = new Visit();
            $visit->post_id = $post_id;
            $visit->user_session = $session_id;
            $visit->user_id = $user_id;
            $visit->user_ip = $user_ip;
            $visit->read_at = $read_at;
            
            $visit_old = DB::table('visits')
                ->select('id')
                ->where('post_id',$post_id)
                ->whereDay('read_at', '=', date('d'))
                ->whereMonth('read_at', '=', date('m'))
                ->whereYear('read_at', '=', date('Y'))
                ->where(function ($query) use ($user_id, $session_id) {
                    if($user_id) {
                        $query->where('user_id', $user_id)
                            ->orWhere('user_session', $session_id);
                    }
                    else {
                        $query->whereNull('user_id')
                            ->where('user_session', $session_id);
                    }
        
                })
                ->first();
            if ($visit_old && is_object($visit_old)) {
                $data = array(
                    'status'    => 'success',
                    'code'      => 200,
                    'post_id'   => $post_id,
                    'message'   => 'Visita ya registrada en el día actual'
                );
    
            }
            else {
                $visit->save();
                $data = array(
                    'status'    => 'success',
                    'code'      => 200,
                    'post_id'   => $post_id,
                    'message'   => 'Visita registrada'
                );
            }

        }
        else {
            $data = array(
                'status'    => 'error',
                'code'      => 403,
                'message'   => 'Entrada no encontrada.'
            );

        }

        return response()->json($data, (isset($data['code']))? $data['code'] : 200 );

    }
    

    static public function getIp(){
        foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR') as $key){
            if (array_key_exists($key, $_SERVER) === true){
                foreach (explode(',', $_SERVER[$key]) as $ip){
                    $ip = trim($ip); // just to be safe
                    if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) !== false){
                        return $ip;
                    }
                }
            }
        }
    }
}