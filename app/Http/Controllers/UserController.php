<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\User;

class UserController extends Controller
{
    public function register(Request $request){

        // Recoger los datos del usuario por post
        $json = $request->input('json', null);
        $params = json_decode($json, true);

        if(!empty($params)){
            // Limpiar espacios sobrantes en los datos
            $params = array_map('trim', $params);

            // Validar los datos
            $validate = \Validator::make($params, [
                'name'      => 'required|regex:/^([a-zA-ZñÑáéíóúÁÉÍÓÚ_-])+((\s*)+([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-]*)*)+$/|max:255',
                'surname'   => 'required|regex:/^([a-zA-ZñÑáéíóúÁÉÍÓÚ_-])+((\s*)+([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-]*)*)+$/|max:255',
                'email'     => 'required|email|max:255|unique:users',
                'password'  => 'required|min:6|max:16',
                'image'     => 'nullable|max:255'
            ]);

            if($validate->fails()){
                $data = array(
                    'status'    => 'error',
                    'code'      => 400,
                    'message'   => 'El usuario no ha sido registrado. Por favor verifique los datos suministrados.',
                    'errors'     => $validate->errors()
                );    
            }
            else{
                // Cifrar la contraseña
                $pwd = hash('sha256', $params['password']);

                // Crear el usuario
                $user = new User();
                $user->name = $params['name'];
                $user->surname = $params['surname'];
                $user->email = $params['email'];
                $user->password = $pwd;
                $user->role = 'ROLE-USER';
                if(in_array('description', $params)){
                    $user->description = $params['description'];
                }
                if(in_array('image', $params)){
                    $user->image = $params['image'];
                }

                // Guardar el usuario
                $user->save();
                
                if($user->save()){
                    $data = array(
                        'status'    => 'success',
                        'code'      => 200,
                        'message'   => 'El usuario ha sido registrado.',
                        'user'      => $user
                    );    
    
                }
                else{
                    $data = array(
                        'status'    => 'error',
                        'code'      => 500,
                        'message'   => 'El usuario no ha podido ser registrado en el sistema. Por favor consulte el administrador del servicio.',
                    );    
    
                }

            }

        }
        else{
            $data = array(
                'status'    => 'error',
                'code'      => 400,
                'message'   => 'Error en el formato de los datos enviados.',
            );

        }


        return response()->json($data, $data['code']);
    }

    public function login(Request $request){

        $json = $request->input('json', null);
        $params = json_decode($json, true);
        if(!empty($params)){

            // Validar los datos suministrados
            $validate = \Validator::make($params, [
                'email'     => 'required|email|max:255',
                'password'  => 'required|min:6|max:16',
            ]);

            if($validate->fails()){
                $data = array(
                    'status'    => 'error',
                    'code'      => 400,
                    'message'   => 'Por favor verifique los datos suministrados.',
                    'errors'     => $validate->errors()
                );    
            }
            else{

                // Validar la autenticidad de la cuenta
                $email = $params['email'];
                $password = hash('sha256', $params['password']);
    
                $jwtAuth = new \JwtAuth();
                if(isset($params['gettoken'])){
                    $data = $jwtAuth->signup($email, $password, $params['gettoken']);
                }
                else{
                    $data = $jwtAuth->signup($email, $password);
                }

            }

        }
        else{
            $data = array(
                'status'    => 'error',
                'code'      => 400,
                'message'   => 'Error en el formato de los datos enviados.',
            );

        }

        return response()->json($data, (isset($data['code']))? $data['code'] : 200 );
    }

    public function update(Request $request){

        $token = $request->header('Authorization');
        $jwtAuth = new \JwtAuth();
        $identity = $jwtAuth->checkToken($token, true);
        if($identity){
            $user_id = $identity->sub;

            // Recoger los datos del usuario por post
            $json = $request->input('json', null);
            $params = json_decode($json, true);

            if(!empty($params)){
                // Limpiar espacios sobrantes en los datos
                $params = array_map('trim', $params);

                // Validar los datos
                $validate = \Validator::make($params, [
                    'name'      => 'required|regex:/[a-zA-Z\p{Latin}\s]+/|max:255',
                    'surname'   => 'required|regex:/[a-zA-Z\p{Latin}\s]+/|max:255',
                    'email'     => 'required|email|max:255|unique:users,email,'.$user_id,
                    'image'     => 'nullable|max:255'
                ]);

                if($validate->fails()){
                    $data = array(
                        'status'    => 'error',
                        'code'      => 400,
                        'message'   => 'El usuario no ha sido actualizado. Por favor verifique los datos suministrados.',
                        'errors'     => $validate->errors()
                    );    
                }
                else{
                    // Limpiar datos que no deben actualizarse
                    unset($params['id']);
                    unset($params['password']);
                    unset($params['role']);
                    unset($params['remember_token']);

                    // Actualizar el usuario
                    $user_update = User::where('id', $user_id)->update($params);

                    if($user_update){
                        $data = array(
                            'status'    => 'success',
                            'code'      => 200,
                            'message'   => 'El usuario ha sido actualizado.',
                            'user'      => $params
                        );    
    
                    }
                    else{
                        $data = array(
                            'status'    => 'error',
                            'code'      => 500,
                            'message'   => 'El usuario no ha podido ser actualizado en el sistema. Por favor consulte el administrador del servicio.',
                        );    
    
                    }

                }
            }

        }
        else{
            $data = array(
                'status'    => 'error',
                'code'      => 401,
                'message'   => 'Error, necesita una sesion de usuario activa para esta accion.',
            );

        }

        return response()->json($data, (isset($data['code']))? $data['code'] : 200 );
    }

    public function upload(Request $request){
        // Recoger los datos de la petición
        $image = $request->file('file0');

        // Validar la imagen
        $validate = \Validator::make($request->all(),[
            'file0' => 'required|image|mimes:jpg,jpeg,png,gif'
        ]);

        if(!$image || $validate->fails()){
            $data = array(
                'status'    => 'error',
                'code'      => 400,
                'message'   => 'Error al subir la imagen.',
                'errors'     => $validate->errors()
            );
        }
        else{
            // Guardar la imagen
            $image_name = time().$image->getClientOriginalName();
            \Storage::disk('users')->put($image_name, \File::get($image));

            // Devolver la ubicación de la imagen
            $data = array(
                'status'    => 'success',
                'code'      => 200,
                'message'   => 'Archivo cargado exitosamente.',
                'image'     => $image_name,
            );

        }

        return response()->json($data, (isset($data['code']))? $data['code'] : 200 );
    }

    public function getImage($filename){
        // Validar si existe el archivo
        $isset = \Storage::disk('users')->exists($filename);

        if($isset){
            // Entregar flujo de archivo
            $file = \Storage::disk('users')->get($filename);

            return new Response($file, 200);    
        }
        else{
            // Enviar mensaje de error
            $data = array(
                'status'    => 'error',
                'code'      => 404,
                'message'   => 'Archivo no encontrado.',
            );
            return response()->json($data, (isset($data['code']))? $data['code'] : 200 );
        }

    }

    public function detail($id){
        //Buscar usuario
        $user = User::find($id);

        if(is_object($user)){
            $data = array(
                'status'    => 'success',
                'code'      => 200,
                'message'   => 'Usuario enconrado.',
                'user'     => $user,
            );

        }
        else{
            $data = array(
                'status'    => 'error',
                'code'      => 404,
                'message'   => 'Usuario no encontrado.',
            );

        }
        return response()->json($data, (isset($data['code']))? $data['code'] : 200 );
    }

}
