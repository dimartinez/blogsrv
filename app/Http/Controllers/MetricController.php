<?php

namespace App\Http\Controllers;

use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

use App\Post;
use App\Comment;

class MetricController extends Controller {

    public function __construct() {
        $this->middleware('api.auth', ['except' => []]);
    }

    public function getMetricsByCourse($course_id, $nb_days = 30) {
        // Validación de la entrada de datos
        if (!is_numeric($nb_days) || $nb_days > 90 || $nb_days < 1) {
            $data = array(
                'status'    => 'error',
                'code'      => 404,
                'message'   => 'Error en la especificación de días de la consulta. Se espera un número no superior a 90 días'
            );    

        }
        else {
            
            $posts = self::_getRecentPostsByReference($course_id, 'course', $nb_days);
            $posts = array_map(function ($x) { return $x->id; }, $posts);
            $posts_count_recent = count($posts);
            $comments = self::_getRecentCommentsByPosts($posts, $nb_days);
            $comment_count_recent = count($comments);

            unset($posts);
            $posts = self::_getPostsByReference($course_id, 'course');
            $post_last_date = (is_array($posts) && isset($posts[0]->updated_at))? $posts[0]->updated_at : null;
            $posts = array_map(function ($x) { return $x->id; }, $posts);
            $posts_count_total = count($posts);
            $comments = self::_getCommentsByPosts($posts);
            $comment_last_date = (is_array($comments) && isset($comments[0]->updated_at))? $comments[0]->updated_at : null;
            $comment_count_total = count($comments);

            $data = array(
                'status'            => 'success',
                'code'              => 200,
                'course_id'         => $course_id,
                'nb_days'           => $nb_days,
                'recent_nb_post'    => $posts_count_recent,
                'recent_nb_comments'=> $comment_count_recent,
                'total_nb_posts'    => $posts_count_total,
                'total_nb_comments' => $comment_count_total,
                'last_post_date'    => $post_last_date,
                'last_comment_date' => $comment_last_date,
            );    

        }

        return response()->json($data, (isset($data['code']))? $data['code'] : 200 );
    }

    private static function _getRecentPostsByReference($ref_id, $ref_model = 'course', $nb_days = 30) {
        $posts = DB::table('posts')
            ->join('categories','posts.category_id', '=', 'categories.id')
            ->select('posts.id', 'posts.updated_at')
            ->distinct()
            ->where('posts.updated_at', '>=', date('Y-m-d', strtotime("- $nb_days day")))
            ->where(function ($query) { $query->whereNull('posts.due_date')->orWhere('posts.due_date', '>=', date('Y-m-d')); })
            ->where('categories.ref_model', $ref_model)
            ->where('categories.ref_id', $ref_id)
            ->orderBy('posts.updated_at', 'DESC')
            ->get()
            ->toArray();
        return $posts;
    }

    private static function _getRecentCommentsByPosts($posts, $nb_days = 30) {
        $comments_count = DB::table('comments')
            ->select('id', 'updated_at')
            ->where('status', 1)
            ->where('comments.updated_at', '>=', date('Y-m-d', strtotime("- $nb_days day")))
            ->whereIn('post_id', $posts)
            ->orderBy('updated_at', 'DESC')
            ->get()
            ->toArray();

        return $comments_count;
    }

    private static function _getPostsByReference($ref_id, $ref_model = 'course') {
        $posts = DB::table('posts')
            ->join('categories','posts.category_id', '=', 'categories.id')
            ->select('posts.id', 'posts.updated_at')
            ->distinct()
            ->where(function ($query) { $query->whereNull('posts.due_date')->orWhere('posts.due_date', '>=', date('Y-m-d')); })
            ->where('categories.ref_model', $ref_model)
            ->where('categories.ref_id', $ref_id)
            ->orderBy('posts.updated_at', 'DESC')
            ->get()
            ->toArray();
        return $posts;
    }

    private static function _getCommentsByPosts($posts) {
        $comments_count = DB::table('comments')
            ->select('id', 'updated_at')
            ->where('status', 1)
            ->whereIn('post_id', $posts)
            ->orderBy('updated_at', 'DESC')
            ->get()
            ->toArray();

        return $comments_count;
    }

}