<?php
namespace App\Helpers;

use Firebase\JWT\JWT;
use Illuminate\Support\Facades\DB;
use App\User;

class JwtAuth {

    public $key;

    public function __construct(){
        $this->key = 'RmNn-EXTvZF5W=H-LY*Rr9yc$vwp&9BV';
    }

    public function signup($email, $password, $gettoken = null){
        // Buscar si existe el usuario con las credenciales
        $user = User::where([
            'email' => $email,
            'password' => $password
        ])->first();

        // Comprobar si son correctas
        $signup = false;
        if(is_object($user)){
            $signup = true;
        }

        // Generar el token con los datos del usuario
        if($signup){
            $token = array(
                'iss'       => 'blogsrv',
                'aud'       => 'myblog',
                'sub'       => $user->id,
                'email'     => $user->email,
                'name'      => $user->name,
                'surname'   => $user->surname,
                'role'      => $user->role,
                'description'=> $user->description,
                'image'     => $user->image,
                'iat'       => time(),
                'exp'       => time() + (30*24*60*60),
            );

            $jwt = JWT::encode($token, $this->key, 'HS256');
            if($gettoken == true || $gettoken == 'true'){
                $data = $token;
            }
            else{
                $data = $jwt;
            }
        }
        else{
            $data = array(
                'status'    => 'error',
                'code'      => 403,
                'message'   => 'Login incorrecto.',
            );

        }

        // Devolver los datos decodificados o el token, en funcion de un parametro
        return $data;
    }

    public function checkToken($jwt, $getIdentity = false){

        $auth = false;

        try {
            $jwt = str_replace('"', '', $jwt);
            $decoded = JWT::decode($jwt, $this->key, ['HS256']);
        }
        catch (\UnexpectedValueException $e) {
            $auth = false;
        }
        catch (\DomainException $e) {
            $auth = false;
        }

        if(!empty($decoded) && is_object($decoded) && isset($decoded->sub)){
            $auth = true;
        }
        else{
            $auth = false;
        }

        if($getIdentity && $auth){
            $auth = $decoded;
        }

        return $auth;
    }

}