<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
    protected $table = 'likes';

    //Relación uno a muchos inversa (muchos a uno)
    public function comment(){
        return $this->belongsTo('App\Comment', 'comment_id');
    }

    public function user(){
        return $this->belongsTo('App\User', 'user_id');
    }
}
