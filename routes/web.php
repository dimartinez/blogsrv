<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Middleware\ApiAuthMiddleware;

Route::get('/', function () {
    return view('welcome');
});

Route::post('/api/register', 'UserController@register');
Route::post('/api/login', 'UserController@login');
Route::put('/api/user/update', 'UserController@update');
Route::post('/api/user/upload', 'UserController@upload')->middleware(ApiAuthMiddleware::class);
Route::get('/api/user/avatar/{filename}', 'UserController@getImage');
Route::get('/api/user/detail/{id}', 'UserController@detail');

Route::resource('/api/category', 'CategoryController');
Route::resource('/api/post', 'PostController');
Route::get('/api/post/bycategory/{id}', 'PostController@postsByCategory');
Route::get('/api/post/byuser/{id}', 'PostController@postsByUser');
Route::get('/api/post/bycourse/{course_id}/{nb_days?}', 'PostController@postsByCourse');
Route::get('/api/post_inactives', 'PostController@indexInactives');
Route::post('/api/post/upload', 'PostController@upload');
Route::get('/api/post/image/{filename}', 'PostController@getImage');

Route::get('/api/post/{post_id}/comment', 'CommentController@getComments');
Route::put('/api/post/{post_id}/comment', 'CommentController@newComment');
Route::get('/api/comment/{comment_id}', 'CommentController@getComment');
Route::get('/api/comment/{comment_id}/response', 'CommentController@getCommentsByParent');
Route::delete('/api/comment/{comment_id}', 'CommentController@destroyComment');

Route::get('/api/post/{post_id}/stars', 'PostController@getAverageStars');
Route::put('/api/post/{post_id}/stars', 'PostController@updateMyStars');

Route::get('/api/comment/{comment_id}/likes', 'CommentController@getSumCommentLikes');
Route::put('/api/comment/{comment_id}/likes', 'CommentController@likeComment');
Route::delete('/api/comment/{comment_id}/likes', 'CommentController@unlikeComment');

Route::get('/api/metric/bycourse/{course_id}/{nb_days?}/{course_name?}', 'MetricController@getMetricsByCourse');

Route::post('/api/reference/newcourse', 'CategoryController@storeReferenceCourse');
Route::post('/api/post/{post_id}/visit', 'VisitController@storeVisit');

// Warning: this route match with others routes up.
Route::get('/api/post/{id}/{dir}', 'PostController@postNext');
