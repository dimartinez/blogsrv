# blogsrv
Backend en PHP sobre Laravel + MySQL para el cliente web myblog sobre angular, encargado de tramitar las entradas al blog de formación institucional.

El despliegue automático en el servidor de desarrollo es una herramienta disponible para facilitar las pruebas del desarrollador sobre una máquina remota dispuesta para tal fin. Tener en cuenta que esto no realiza ninguna acción de compilación o preparación automática del código.

Despliegue sobre el remoto en:
## Servidor remoto
- Crear una carpeta para desplegar en el servidor de desarrollo. Esta carpeta contendrá el código fuente requerido para la publicación de la aplicación. Las instrucciones de publicación del sitio web están por fuera del alcance de esta guía.
> mkdir projects/blogsrv

- Agregar un repositorio desnudo (bare) en el servidor de desarrollo
> mkdir repos
> cd ./repos
> git init --bare blogsrv.git

- Crear hook al recibir actualizaciones del repositorio
> cd ./blogsrv.git/hooks
> vi post-receive
```
#!/bin/bash
TARGET="/home/developer/projects/blogsrv"
GIT_DIR="/home/developer/repos/blogsrv.git"
BRANCH="master"

while read oldrev newrev ref
do
	# only checking out the master (or whatever branch you would like to deploy)
	if [ "$ref" = "refs/heads/$BRANCH" ];
	then
		echo "Ref $ref received. Deploying ${BRANCH} branch to production..."
		git --work-tree=$TARGET --git-dir=$GIT_DIR checkout -f $BRANCH
	else
		echo "Ref $ref received. Doing nothing: only the ${BRANCH} branch may be deployed on this server."
	fi
done
```

- Convertir el archivo post-receive en un ejecutable:
> chmod +x post-receive



## Repositorio local:
- Crear llave ssh en equipo local. Usar los nombres de archivo sugeridos. Cuando se le solicite, asignar una contraseña para usar el ssh-key:
> ssh-keygen -t rsa

- Registrar la llave pública en gitlab bajo la url del usuario: https://gitlab.metadockit.com/profile/keys
  - Copiar y pegar el contenido del archivo 'id_rsa.pub' en el registro del 'key' de ssh del usuario.

- Registrar la llave pública en el servidor remoto de desarrollo del usuario developer sobre el host 192.168.0.109
> cat ~/.ssh/id_rsa.pub | ssh developer@192.168.0.109 "mkdir -p ~/.ssh && touch ~/.ssh/authorized_keys && chmod -R go= ~/.ssh && cat >> ~/.ssh/authorized_keys"

- Crear repositorio local de trabajo
> cd /users/diego/
> git clone git@gitlab.metadockit.com:tq-dev/qms/blogsrv.git

- Crear origen remoto sobre repositorio local del proyecto. Aquí el remoto se le denomina 'test' y los push deberían hacerse sobre la rama master.
> cd ./blogsrv
> git remote add test developer@192.168.0.109:repos/blogsrv.git


## Publicación de cambios en el servidor remoto de desarrollo
- Luego de efectuar los cambios localmente, usar la instrucción ´git push´ indicando el remoto 'test' y la rama 'master' para reflejar los cambios en el servidor remoto:
> git push test master